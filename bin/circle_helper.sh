#!/bin/bash

set -euo pipefail

export CIRCLE_WORKDIR="circle-workdir"
export AWS_DEFAULT_REGION="${ECS_DEPLOY_AWS_DEFAULT_REGION}"
export AWS_ACCESS_KEY_ID="${ECS_DEPLOY_AWS_ACCESS_KEY_ID}"
export AWS_SECRET_ACCESS_KEY="${ECS_DEPLOY_AWS_SECRET_ACCESS_KEY}"

uname=$(uname)
MD5_UTIL=''
if [[ "${uname}" == 'Darwin' ]]; then
    MD5_UTIL='md5'
else
    MD5_UTIL='md5sum'
fi
export MD5_UTIL

# By default the REPO_NAME is the NAMESPACE combined with the APP_NAME
# To override this behaviour REPO_NAME_OVERRIDE can be set to use REPO_NAME_OVERRIDE instead of the APP_NAME

if [ -z ${REPO_NAME_OVERRIDE+x} ]; then
  export REPO_NAME="${NAMESPACE}-${APP_NAME}"
else
  export REPO_NAME="${REPO_NAME_OVERRIDE}"
fi

export ECR_REPO_PATH="${ECR_REPO_BASE}/${REPO_NAME}"


function login() {
    aws ecr get-login --no-include-email
}

function create_workdir() {
    mkdir -p ./${CIRCLE_WORKDIR}
}

function create_hash_cache_file() {
    ${MD5_UTIL} < ./${HASH_CACHE_FILE} | awk '{print $1}' > ./${CIRCLE_WORKDIR}/md5sum
}

function ecr_image_exist() {
    if aws ecr describe-images --repository-name=${REPO_NAME} --image-ids=imageTag=${1} --region ${ECS_DEPLOY_AWS_DEFAULT_REGION} > /dev/null 2> /dev/null; then
      # In unix 0 is positive
      return 0
    fi
    # In unix 1 is negative
    return 1
}

function circle_build_and_push() {
  create_workdir
  create_hash_cache_file

  export MD5SUM=$(cat ./${CIRCLE_WORKDIR}/md5sum)

  BUILD_ARGS=""
  BUILD_ARGS_MD5SUM=""
  CIRCLE_BRANCH_WITHOUT_SLASHES=${CIRCLE_BRANCH//\//_}

  if egrep -q "^ARG [0-9a-zA-Z\_\-]+$" ./Dockerfile ;then
    ARGS=$(cat ./Dockerfile |grep "^ARG "|grep -v "=" | sed 's/ARG //g'|xargs )
  fi

  if [ -z ${ARGS+x} ]; then
    echo "- no ARG defined in Dockerfile;"
  else
    echo "ARG defined in Dockerfile";
    for i in $ARGS
    do
      HAS_VAL=$(eval echo $(printf '${%s+1}' $i))
      if [ -z $HAS_VAL ]; then
          echo "WARNING: env var >${i}< is not set. Dockerfile ARG will be unset"
      else
          echo "INFO: using env var >${i}< is as build-arg"
          VAL=$(eval echo '$'$i)
          BUILD_ARGS+=" --build-arg $i=$VAL "
            # remove trailing ' -' with awk, in case md5um has been used
          BUILD_ARGS_MD5SUM="-$(echo "${BUILD_ARGS}" | ${MD5_UTIL} | awk '{print $1}')"
      fi
    done
    echo "BUILD_ARGS: ${BUILD_ARGS}"
  fi

  export BUILD_ARGS_MD5SUM
  echo "$BUILD_ARGS_MD5SUM" > ./${CIRCLE_WORKDIR}/build_args_md5sum

  if ! ecr_image_exist "build-${CIRCLE_SHA1}${BUILD_ARGS_MD5SUM}";then
    CACHE_FROM=""
    if docker pull ${ECR_REPO_PATH}:build-${MD5SUM}; then
       CACHE_FROM="--cache-from ${ECR_REPO_PATH}:build-${MD5SUM}"
    elif docker pull ${ECR_REPO_PATH}:latest; then
       CACHE_FROM="--cache-from ${ECR_REPO_PATH}:latest"
    fi

    echo "build $BUILD_ARGS ${CACHE_FROM} -t ${ECR_REPO_PATH}:build-${CIRCLE_SHA1}${BUILD_ARGS_MD5SUM} ."
    docker build $BUILD_ARGS ${CACHE_FROM} -t ${ECR_REPO_PATH}:build-${CIRCLE_SHA1}${BUILD_ARGS_MD5SUM} .
    docker push ${ECR_REPO_PATH}:build-${CIRCLE_SHA1}${BUILD_ARGS_MD5SUM}
  fi
  aws ecr batch-get-image --repository-name ${REPO_NAME} --image-ids imageTag=build-${CIRCLE_SHA1}${BUILD_ARGS_MD5SUM} --query 'images[].imageManifest' --output text > ./${CIRCLE_WORKDIR}/manifest
}

function retag_image_to_stage() {
    if ! ecr_image_exist "${STAGE}-${CIRCLE_SHA1}"; then
      aws ecr put-image --repository-name ${REPO_NAME} --image-tag ${STAGE}-${CIRCLE_SHA1} --image-manifest "$(cat ./${CIRCLE_WORKDIR}/manifest)"
      aws ecr put-image --repository-name ${REPO_NAME} --image-tag ${STAGE} --image-manifest "$(cat ./${CIRCLE_WORKDIR}/manifest)"
    fi
 }

function retag_image_to_latest() {
  echo "Tagging the build image to ${REPO_NAME}:$(cat ./${CIRCLE_WORKDIR}/md5sum )"
  if ! aws ecr put-image --repository-name ${REPO_NAME} --image-tag "build-$(cat ./${CIRCLE_WORKDIR}/md5sum )" --image-manifest "$(cat ./${CIRCLE_WORKDIR}/manifest)"; then
    echo "Tagging the build image to ${REPO_NAME}:latest"
    aws ecr put-image --repository-name ${REPO_NAME} --image-tag latest --image-manifest "$(cat ./${CIRCLE_WORKDIR}/manifest)"
  fi
}

function deploy_docker() {
    /ecs-deploy -c $CLUSTER -n ${SERVICE_NAME} -i ${ECR_REPO_PATH}:${STAGE}-$CIRCLE_SHA1 -t 400 -a $DEPLOY_ROLE
 }

if [ "$1" == "login" ]; then
  login
elif [ "$1" == "circle_build_and_push" ]; then
  circle_build_and_push
elif [ "$1" == "retag_image_to_latest" ]; then
  retag_image_to_latest
elif [ "$1" == "retag_image_to_stage" ]; then
  retag_image_to_stage
elif [ "$1" == "deploy_docker" ]; then
  deploy_docker
else
    echo "Unknown commmand: $1"
    exit 1;
fi
